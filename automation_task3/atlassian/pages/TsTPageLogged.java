package atlassian.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import atlassian.util.PageHelper;

public class TsTPageLogged {
  private WebDriver driver;
	
  public TsTPageLogged(WebDriver driver) {
    this.driver = driver;
    PageFactory.initElements(driver, this);
  }
	
  /*
   *  create button
   */
  @FindBy(how = How.ID, using = "create_link")
  WebElement create_button;
	
  /*
   *  issues drop down list
   */
  @FindBy(how = How.ID, using = "find_link")
  WebElement issues_dropDownList;
	
  /*
   *  drop down options for issues (I only add "reported by me" for test purpose.
   */
  @FindBy(how = How.ID, using = "filter_lnk_reported")
  WebElement reportedByMe_option;
	
  /*
   *   hint message
   */
  @FindBy(how = How.XPATH, using = "//a[@class = 'issue-created-key issue-link']")
  WebElement hintMessage_text;
  
  /*
   *  close hint message
   */
  @FindBy(how = How.XPATH, using = "//span[@class = 'aui-icon icon-close']")
  WebElement closeHintMessage_icon;
	
	
  public CreateIssueWindow createIssue() {
    create_button.click();
    PageHelper.waitCreateIssueWindowOpen(driver);
    return new CreateIssueWindow(driver);
  }
	
  public ReportedByMePage searchIssuesReportedByMe() {
    issues_dropDownList.click();
    PageHelper.waitIssuesDropDownListExpand(driver);
    reportedByMe_option.click();
    PageHelper.reportedByMePageLoading(driver);
    return new ReportedByMePage(driver);
  }
	
  /*
   * get hint message
   */
  public String getHintMessage() {
    return hintMessage_text.getText();
  }
  
  /*
   * close hint message
   */
  public void closeHintMessage() {
    PageHelper.waitIssueCreatedHintAppear(driver);
    closeHintMessage_icon.click();
  }
	
}
