package atlassian.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import atlassian.util.PageHelper;

public class TstPage {
  private WebDriver driver;
	
  public TstPage(WebDriver driver) {
    this.driver = driver;
    driver.get(PageHelper.loginUrl);
    PageFactory.initElements(driver, this);
  }
	
  /*
   *   "Log in" Element
   */
  @FindBy(how = How.ID, using = "user-options")
  WebElement logIn_Element;
	
  /*
   * login 
   */
  public JiraSignInPage clickLogin() {
    logIn_Element.click();
    PageHelper.waitJiraLoginPageLoading(driver);
    return new JiraSignInPage(driver);
  }
}