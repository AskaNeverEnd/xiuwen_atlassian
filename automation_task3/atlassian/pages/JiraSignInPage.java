package atlassian.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import atlassian.util.PageHelper;

public class JiraSignInPage {
  private WebDriver driver;
	
  public JiraSignInPage(WebDriver driver) {
    this.driver = driver;
    PageFactory.initElements(driver, this);
  }
	
  /*
   *  email address field
   */
  @FindBy(how = How.XPATH, using = "//input[@id = 'username']")
  WebElement email_inputField;
	
  /*
   *  password field
   */
  @FindBy(how = How.ID, using = "password")
  WebElement password_inputField;
	
  /*
   * sign in button
   */
  @FindBy(how = How.ID, using = "login-submit")
  WebElement signIn_Button;
	
  /*
   *  more webElements need be added
   */
  public void inputEmail(String email) {
    email_inputField.sendKeys(email);
  }
	
  public void inputPassWord(String pwd) {
    password_inputField.sendKeys(pwd);
  }
	
  public TsTPageLogged login(String email, String pwd) {
    inputEmail(email);
    inputPassWord(pwd);
    signIn_Button.click();
    PageHelper.waitTstPageLoging(driver);
    return new TsTPageLogged(driver);
  }
}
