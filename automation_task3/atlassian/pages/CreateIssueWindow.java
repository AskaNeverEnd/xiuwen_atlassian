package atlassian.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class CreateIssueWindow {
  private WebDriver driver;
	
  public CreateIssueWindow(WebDriver driver) {
    this.driver = driver;
    PageFactory.initElements(driver, this);
  }
	
  /*
   *  summary field
   */
  @FindBy(how = How.ID, using = "summary")
  WebElement summary_inputField;
	
  /*
   * Create button
   */
  @FindBy(how = How.ID, using = "create-issue-submit")
  WebElement create_button;
	
  /*
   *  input summary
   */
  public void setSummary(String summary) {
    summary_inputField.sendKeys(summary);
  }
	
  /*
   * create an issue
   */
  public void createissue() {
    create_button.click();
  }
}
