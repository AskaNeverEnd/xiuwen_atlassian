package atlassian.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class ReportedByMePage {
  private WebDriver driver;
  
  public ReportedByMePage(WebDriver driver) {
    this.driver = driver;
    PageFactory.initElements(driver, this);
  }
  
  /*
   *  drop down options for issues (I only add "reported by me" for test purpose.
   */
  @FindBy(how = How.CLASS_NAME, using = "search-title")
  WebElement pageTitle_text;
  
  /*
   * find issue name existed or not.
   */
  public boolean issueExisted(String issueName) {
    List<WebElement> elements = driver.findElements(By.xpath("//ol[@class = 'issue-list']/li"));
    for (WebElement element : elements) {
      if (element.getAttribute("title").equals(issueName)){
        return true;
      }
    }
    return false;
  } 
}
