package atlassian.util;

import java.net.URL;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import atlassian.pages.JiraSignInPage;
import atlassian.pages.TsTPageLogged;
import atlassian.pages.TstPage;

public class PageHelper {
  public static final String loginUrl = "https://jira.atlassian.com/browse/TST";
  public static final String userName = "zhongxiuwen61@gmail.com";
  public static final String password = "atlassian@123";

  /*
   *  time waiting period value
   */
  public static final int SHORT_TIME = 5;
  public static final int MEDIUM_TIME = 15;
  public static final int LONG_TIME = 50;
	
  /*
   *   waiting page loaded mark for different page
   *   entertainmentPageXpath is a example
   */
  public static String tstPageMarkById = "user-options"; 
  public static String jiraSignInPageMarkById = "login-submit";
  public static String tstPageLoggedMarkById = "create_link";
  public static String CreateIssueWindowMarkByClass = "jira-dialog-heading";
  public static String issuesDropDownListMarkId = "filter_lnk_reported";
  public static String issueCreateMarkXpath = "//a[@class = 'issue-created-key issue-link']";
  public static String reportedByMePageMarkClass = "search-title";
	
	
  /*
   * help function: waiting page load
   */
  public static void waitForElement(WebDriver driver, By by, long timeOutInSeconds) {
    WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);
    wait.until(ExpectedConditions.visibilityOfElementLocated(by));
  }
	 
  public static void waitTstPageLoading(WebDriver driver) {
    waitForElement(driver, By.id(tstPageMarkById), LONG_TIME);
  }
	
  public static void waitJiraLoginPageLoading(WebDriver driver) {
    waitForElement(driver, By.id(jiraSignInPageMarkById), LONG_TIME);
  }
	
  public static void waitTstPageLoging(WebDriver driver){
    waitForElement(driver, By.id(tstPageLoggedMarkById), LONG_TIME);
  }
  
  public static void waitCreateIssueWindowOpen(WebDriver driver){
    waitForElement(driver, By.className(CreateIssueWindowMarkByClass), LONG_TIME);
  }
  
  public static void waitIssuesDropDownListExpand(WebDriver driver){
    waitForElement(driver, By.id(issuesDropDownListMarkId), LONG_TIME);
  }
  
  public static void waitIssueCreatedHintAppear(WebDriver driver){
    waitForElement(driver, By.xpath(issueCreateMarkXpath), LONG_TIME);
  }
  
  public static void reportedByMePageLoading(WebDriver driver){
    waitForElement(driver, By.className(reportedByMePageMarkClass), LONG_TIME);
  }
  
  /*
   *  login test
   */
  public static TsTPageLogged loginTstPage(WebDriver driver) {
    TstPage tstPage = new TstPage(driver);
    PageHelper.waitTstPageLoading(driver);
    JiraSignInPage loginPage = tstPage.clickLogin();
    return loginPage.login(PageHelper.userName, PageHelper.password);
  }
  
  /*
   *  the following part is very cool ! we use browserstack to test different
   *  browser and OS.
   */
  public static final String browserStack = "http://xiuwenatlas1:xbeXUstS153AdzzGBgzd@hub.browserstack.com/wd/hub";
  
  /*
	 *    different test environment setting
	 */
  public static WebDriver winXpFireFox26() throws Exception {
    DesiredCapabilities caps = new DesiredCapabilities();
    caps.setCapability("browser", "firefox");
    caps.setCapability("browser_version", "26.0");
    caps.setCapability("os", "Windows");
    caps.setCapability("os_version", "XP");
    caps.setCapability("browserstack.debug", "true");
    WebDriver driver = new RemoteWebDriver(new URL(browserStack), caps);
    return driver;
  }
	
  public static WebDriver win7Safari() throws Exception {
    DesiredCapabilities caps = new DesiredCapabilities();
    caps.setCapability("browser", "safari");
    caps.setCapability("browser_version", "5.1");
    caps.setCapability("os", "Windows");
    caps.setCapability("os_version", "7");
    caps.setCapability("browserstack.debug", "true");
    WebDriver driver = new RemoteWebDriver(new URL(browserStack), caps);
    return driver;
  }
	
  public static WebDriver macLionChrome() throws Exception {
    DesiredCapabilities caps = new DesiredCapabilities();
    caps.setCapability("browser", "chrome");
    caps.setCapability("browser_version", "32");
    caps.setCapability("os", "OS X");
    caps.setCapability("os_version", "Lion");
    caps.setCapability("browserstack.debug", "true");
    WebDriver driver = new RemoteWebDriver(new URL(browserStack), caps);
    return driver;
  }
	
}