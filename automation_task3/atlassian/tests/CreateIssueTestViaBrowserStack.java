package atlassian.tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import atlassian.pages.CreateIssueWindow;
import atlassian.pages.ReportedByMePage;
import atlassian.pages.TsTPageLogged;
import atlassian.util.PageHelper;


public class CreateIssueTestViaBrowserStack {
  private WebDriver driver;
	
  @Before
  public void setUp() throws Exception{
    driver = PageHelper.win7Safari();
  }
	
  @After
  public void tearDown(){
    driver.quit();
  }
	
  /*
   *  test created issue can be found in the reported list.
   */
  @Test
  public void testSearchCreatedIssue(){
    TsTPageLogged loggedPage = PageHelper.loginTstPage(driver);
    CreateIssueWindow createWindow = loggedPage.createIssue();
    createWindow.setSummary("hello World");
    createWindow.createissue();
    PageHelper.waitIssueCreatedHintAppear(driver);
    loggedPage.closeHintMessage();
    ReportedByMePage page = loggedPage.searchIssuesReportedByMe();
    assertTrue(page.issueExisted("hello world"));
  }
	
	
  /*
   *  test created issue hint message is right.
   */
  @Test
  public void testHintMessageForCreateIssue(){
    TsTPageLogged loggedPage = PageHelper.loginTstPage(driver);
    CreateIssueWindow createWindow = loggedPage.createIssue();
    createWindow.setSummary("Hint Test");
    createWindow.createissue();
    PageHelper.waitIssueCreatedHintAppear(driver);
    assertTrue(loggedPage.getHintMessage().contains("Hint Test"));
  }
	
}