The task is to write a automated test that new issues can be created in JIRA. 

Because "https://jira.atlassian.com/browse/TST" need user to login, I write extra page objects to 
finish this task. 

If we want to test a new issue can be generated in jira, there are a lot of test cases can be test,
such as: differnet issue type, different project, different assinge,etc. Then we have to verify 
these field are the same as we entered. But here I only input a summary to make sure the issue can
be crated. And I choose two ways to verify that issue has been crated. One is to check the issues 
reported by me search, another one is to check the hint mesage after I created the issue.

We can find that Tstpage, TstPageLogged and REportedByMePage look the same, so the better way is to 
create a absolute father page, then these three pages to inherited it. We can reduce redundancy code
via this way. But time limited, so I did not do that.

In all the page objects, I did not list all the elements and the action for that page. I only add the
elements and actions which I need to use for this task. Because I am using page object pattern, We
can add more details later. 

PageHelper class is used to solve some tricky part for the automation such as waiting particular page
loading, some constant which do not want to expose to public. Also it is a good way to maintain the
test. 

There is a cool thing I want to point out in the PageHelper, I use brwoserstack to solve the multi-
browser and multi-os problem. 

HOW TO RUN MY CODE: 
1. make sure you have install the jdk on your machine
2. create a project in eclipse
3. copy my code into project, please be ware the package name
4. add junit.jar and selenium jar into your project 
if you feel tied for the dependency issue, 
use maven http://maven.apache.org/guides/getting-started/maven-in-five-minutes.html  
5. if you decide to use chrome browser to test, put the chromedriver on your computer and modify
the line 22 in CreateIssueTest.java. if you decide to use other browser, you also need to modify the
same line.
6. run CreateIssueTest.java in Junit.

Wait ? YOU DO NOT WANT TO CONFIG BRWOSER ? HERE WE GO ANOTHER WAY:
1. do the same steps 1 to 4 above
2. open http://www.browserstack.com/  username:99716324@qq.com pwd:eio123456
3. after you sign in, on the top of the page, choose "AUTOMATE"
4. run CreateIssueTestViaBrwoserStack.java in Junit. BUT PLEASE WATCH THE BROWSERSTACK PAGE TO SEE
THE MIRACLE. 
