/**
 * Adds a watcher to all of the supplied issues.
 * If there is partial success, the issues which we can modify will
 * be modified and the ones we cannot will be returned in an ArrayList.
 * @param issues the list of issues to update
 * @param currentUser the user to run the operation as
 * @param watcher the watcher to add to the issues
 * @return an ArrayList<Issue> containing the issues that could not be modified
*/

public ArrayList<Issue> addWatcherToAllModifiedVersion (final ArrayList<Issue> issues, final User
currentUser, final User watcher) {
  ArrayList<Issue> successfulIssues = new ArrayList<Issue> ();
  ArrayList<Issue> failedIssues = new ArrayList<Issue> ();
  for (Issue issue : issues) {
    if (canWatchIssue(issue, currentUser, watcher)) {
      successfulIssues.add (issue);
    }
    else {
      failedIssues.add (issue);   
    }
  }
  if (!successfulIssues.isEmpty()) {                       
    watcherManager.startWatching (currentUser, successfulIssues);
  }
  return failedIssues;
}

private boolean canWatchIssue (Issue issue, User currentUser, User watcher) {
  if (watcher.equals(currentUser) || watcher.getHasPermissionToModifyWatchers()) {   
    return issue.getWatchingAllowed (watcher);
  }
  return false;                         
}


/*
 * Original One have three errors, I have marked them as comments.
 */
public ArrayList<Issue> addWatcherToAll (final ArrayList<Issue> issues, final User
currentUser, final User watcher) {
  ArrayList<Issue> successfulIssues = new ArrayList<Issue> ();
  ArrayList<Issue> failedIssues = new ArrayList<Issue> ();
  for (Issue issue : issues) {
    if (canWatchIssue(issue, currentUser, watcher)) {
      successfulIssues.add (issue);
    }
    else {
      failedIssues.add (issue);   
    }
  }
  if (successfulIssues.isEmpty()) {                       // wrong !!! should add ! 
    watcherManager.startWatching (currentUser, successfulIssues);
  }
  return failedIssues;
}

private boolean canWatchIssue (Issue issue, User currentUser, User watcher) {
  if (currentUser.equals(watcher) || currentUser.getHasPermissionToModifyWatchers()) {  //wrong !!! 
    return issue.getWatchingAllowed (watcher);
  }
  return true;                         //wrong !!! should return false
}




